#ifndef ASTAR_H
#define ASTAR_H

#include <iostream>
#include <math.h>

using namespace std;

const int dir=8; // number of possible directions to go at any position
// if dir==4
//static int dx[dir]={1, 0, -1, 0};
//static int dy[dir]={0, 1, 0, -1};
// if dir==8
static int dx[dir]={1, 1, 0, -1, -1, -1, 0, 1};
static int dy[dir]={0, 1, 1, 1, 0, -1, -1, -1};

class node
{
    // current position
    int xPos;
    int yPos;
    // total distance already travelled to reach the node
    float level;
    // priority=level+remaining distance estimate
    float priority;  // smaller: higher priority

public:
    node(int xp, int yp, float d, float p)
        {xPos=xp; yPos=yp; level=d; priority=p;}

    int getxPos() const {return xPos;}
    int getyPos() const {return yPos;}
    float getLevel() const {return level;}
    float getPriority() const {return priority;}

    void updatePriority(const int & xDest, const int & yDest)
    {
         priority=level+estimate(xDest, yDest); //A*
    }

    // give better priority to going strait instead of diagonally
    void nextLevel(const int & i) // i: direction
    {
         //level+=(dir==8?(i%2==0?10:14):10);
         level+=1;
    }
    
    // Estimation function for the remaining distance to the goal.
    const float & estimate(const int & xDest, const int & yDest) const
    {
        static int xd, yd;
        static float d;
        xd=xDest-xPos;
        yd=yDest-yPos;

        // Euclidian Distance
        d=(sqrt(xd*xd+yd*yd));

        // Manhattan distance
        //d=abs(xd)+abs(yd);
        
        // Chebyshev distance
        //d=max(abs(xd), abs(yd));

        return(d);
    }
};
string pathFind(Graph g, const int & xStart, const int & yStart, const int & xFinish, const int & yFinish);
#endif // ASTAR_H
