#ifndef CAR_H
#define CAR_H

#include <iostream>
#include <string>
#include <cmath>

float WB=3., W=2.; // rear to front wheel, width of car
float LF=3.3, LB=1.0; // distance from rear to vehicle front and back end
float MAX_STEER = 0.6; //[rad] maximum steering angle

float WBUBBLE_DIST = (LF - LB) / 2.0;
float WBUBBLE_R = sqrt(pow((LF + LB) / 2.0, 2.0) + 1);

float MOTION_RESOLUTION = 0.1; // [m] path interporate resolution
float N_STEER = 20.; // number of steer command
float VR = 0.1; // robot radius

class Pose
{
    float x,y,yaw;
    Pose(float in_x, float in_y, float in_yaw)
    {
        x = in_x; y = in_y; yaw = in_yaw;
    }
    float *getPose(void)
    {
        static float pos[3] = {x,y,yaw};
        return pos;
    }
};

class Car
{
    Pose pose;
    bool check_car_collision();
    bool rectangle_check();
    float *move(float *pos, float dist, float steer, float L);
};



#endif
