#ifndef MAP_H
#define MAP_H

#include <iostream>
#include <string>

class Graph
{
public:
    int m, n;
    int **map;
    float XY_GRID_RESOLUTION; // [m]
    float YAW_GRID_RESOLUTION; // [rad]
    Graph(int m, int n);
    void addObstacle(int x, int y) {map[x][y] = 1;}
    void addStart(int x, int y) {map[x][y] = 2;}
    void addRoute(int x, int y) {map[x][y] = 3;}
    void addFinal(int x, int y) {map[x][y] = 4;}
    void addSpace(int x, int y) {map[x][y] = 5;}
    void print();
};
void calc_obstacle_map(Graph g, float res, float rr);
#endif // MAP_H
