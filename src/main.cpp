#include <iostream>
#include "map.h"
#include "Astar.h"
using namespace std;

int main()
{
    int m = 60, n = 60;
    float res = 1., rr = 2.;
    Graph g(n, m);
    
    /*for(int x=n/8;x<n*7/8;x++)
    {
        g.addObstacle(x, m/2);
    }
    for(int y=m/8;y<m*7/8;y++)
    {
        g.addObstacle(n/2, y);
    }*/
    for(int i=0;i<60;i++)
    {
        g.addObstacle(0,i);
        g.addObstacle(i,59);
        g.addObstacle(i,0);
        g.addObstacle(59,i);
    }
    
    for(int i=0;i<40;i++)
    {
        g.addObstacle(20,59-i);
        g.addObstacle(40,i);
    }
    
    int xA, yA, xB, yB;
    srand(time(NULL));
    switch(rand()%8)
    {
        case 0: xA=0;yA=0;xB=n-1;yB=m-1; break;
        case 1: xA=0;yA=m-1;xB=n-1;yB=0; break;
        case 2: xA=n/2-1;yA=m/2-1;xB=n/2+1;yB=m/2+1; break;
        case 3: xA=n/2-1;yA=m/2+1;xB=n/2+1;yB=m/2-1; break;
        case 4: xA=n/2-1;yA=0;xB=n/2+1;yB=m-1; break;
        case 5: xA=n/2+1;yA=m-1;xB=n/2-1;yB=0; break;
        case 6: xA=0;yA=m/2-1;xB=n-1;yB=m/2+1; break;
        case 7: xA=n-1;yA=m/2+1;xB=0;yB=m/2-1; break;
    }
    //xA=n/2-1;yA=m/2-1;xB=n/2+1;yB=m/2+1;
    xA=10; yA=n-10;
    xB=50; yB=n-50;
    
    g.addStart(xA,yA);
    g.addFinal(xB,yB);
    
    cout<<"Map Size (X,Y): "<<n<<","<<m<<endl;
    cout<<"Start: "<<xA<<","<<n-yA<<endl;
    cout<<"Finish: "<<xB<<","<<n-yB<<endl;
    
    calc_obstacle_map(g, res, rr);

    clock_t start = clock();
    string route=pathFind(g, xA, yA, xB, yB);
    if(route=="") cout<<"An empty route generated!"<<endl;
    clock_t end = clock();
    double time_elapsed = double(end - start);
    cout<<"Time to calculate the route (ms): "<<time_elapsed<<endl;
    cout<<"Route:"<<endl;
    cout<<route<<endl<<endl;
    
    g.print();
    
    
    
    return(0);
}
