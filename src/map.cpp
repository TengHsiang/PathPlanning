#include <iostream>
#include <math.h>
#include "map.h"

using namespace std;

Graph::Graph(int m, int n)
{
    this->m = m;
    this->n = n;
    
    map = new int* [n];
    
    for(int i=0;i<n;++i)
    {
        map[i] = new int[m];
        
        memset(map[i], 0, m*sizeof(int));
    }
}

void Graph::print()
{
    for(int y=0;y<n;++y)
    {
        for(int x=0;x<m;++x)
        {
            if(map[x][y]==0)
                cout<<".";
            else if(map[x][y]==1)
                cout<<"O"; //obstacle
            else if(map[x][y]==2)
                cout<<"S"; //start
            else if(map[x][y]==3)
                cout<<"R"; //route
            else if(map[x][y]==4)
                cout<<"F"; //finish
            else if(map[x][y]==5)
                cout<<" "; //config space
        }
        cout<<endl;
    }
}

void calc_obstacle_map(Graph g, float res, float rr)
{
    int xmin = 99999, ymin = 9999;
    int xmax = 0, ymax = 0;
    int xwidth, ywidth;
    Graph g2(g.m, g.n);

    for(int i = 0; i < g.n; i++)
    {
        for(int j = 0; j < g.m; j++) 
        {
            if(g.map[j][i] == 1)
            {
                if(j<xmin) xmin = j;
                if(j>xmax) xmax = j;
                if(i<ymin) ymin = i;
                if(i>ymax) ymax = i;
                g2.map[j][i] = 1;
            }
        } 
    }
    
    xwidth = xmax - xmin + 1;
    ywidth = ymax - ymin + 1;
    cout << "xwidth: " << xwidth << ", ywidth: " << ywidth << endl;

    for(int iy = 0; iy < ywidth; iy++)
    {
        int y = iy + ymin;
        for(int ix = 0; ix < xwidth; ix++) 
        {
            int x = ix + xmin;
            //cout << "x: " << x << ", y: " << y << endl;
            for(int i = 0; i < g.n; i++)
            {
                for(int j = 0; j < g.m; j++) 
                {
                    if(g2.map[j][i] == 1)
                    {
                        float d = sqrt(pow(j-x,2.0)+pow(i-y,2.0));
                        //cout << "x: " << j << ", y: " << i << endl;
                        //cout << "x: " << x << ", y: " << y << endl;
                        //cout << "d: " << d << ", rr/res: " << rr/res << endl;
                        if(d <= (rr/res))
                        {
                            g.addObstacle(x,y);
                            break;
                        }
                    }
                } 
            }
        } 
    }

}